## File downloader
Script to download the file given the URL. It only supports **file, ftp, http, https, jar, mailto, netdoc** protocols as
of now.

### Using script

  
  1. Run the following command from the directory you pulled code to:
  
     ```
     sh filedownloader.sh --urls=<list of url separated by WHITESPACE>
     ```
     Example:
     ```
     sh filedownloader.sh --urls="https://www.google.com https://www.facebook.com"
     ```
     
     File is created at \<base directory\> / \<smart file name\>. Where:
     
     * \<base directory\> is user.home by default, though it can be overriden through --location option.
     * \<smart file name\> is generated such that two different URLs always end up in different files even if 
     downloaded in the same \<base directory\>.
     
     It is generated as per the following format: 
        ```
        /<protocol>/<host>/<port>/<file>
        ```
        
     &nbsp;&nbsp;&nbsp;&nbsp; - \<protocol\> is the scheme mentioned in URL
            
     &nbsp;&nbsp;&nbsp;&nbsp; - \<host\> is the host name (dots are replaced by -)
                 
     &nbsp;&nbsp;&nbsp;&nbsp; - \<port\> is port specified in the URL, if URL does not specifies host explicitly, 
     it's default scheme port.
                  
     &nbsp;&nbsp;&nbsp;&nbsp; - \<file\> file is the rest of the part of URL.
     

### Making modifications to the code

You can modifications to any modifications to the code. Just don't forget to run the following script to update the jar.

```
sh update-jar.sh
```

Feel free to request to push the code to this repository if you want to add a feature or improve existing 
modules. Test your changes through unit and integration tests wherever appropriate. 

### Extending to other protocols

1. Create a custom URLConnection implementation which performs the job in connect() method.
    ```
    public class CustomURLConnection extends URLConnection {
    
        protected CustomURLConnection(URL url) {
            super(url);
        }
    
        @Override
        public void connect() throws IOException {
            // Do your job here. As of now it merely prints "Connected!".
            System.out.println("Connected!");
        }
    
    }
    ```

2. Don't forget to override and implement other methods like getInputStream() accordingly.

    Create a custom URLStreamHandler implementation which returns it in openConnection().
    
    ```
    public class CustomURLStreamHandler extends URLStreamHandler {
    
        @Override
        protected URLConnection openConnection(URL url) throws IOException {
            return new CustomURLConnection(url);
        }
    
    }
    ```

    Don't forget to override and implement other methods if necessary.

3. Create a custom URLStreamHandlerFactory which creates and returns it based on the protocol.

    ```
    public class CustomURLStreamHandlerFactory implements URLStreamHandlerFactory {
    
        @Override
        public URLStreamHandler createURLStreamHandler(String protocol) {
            if ("customuri".equals(protocol)) {
                return new CustomURLStreamHandler();
            }
    
            return null;
        }
    
    }
    ```

    Note that protocols are always lowercase.

4. Register it during application's startup via URL#setURLStreamHandlerFactory()

    ```
    URL.setURLStreamHandlerFactory(new CustomURLStreamHandlerFactory());
    ```
    
    Note that the Javadoc explicitly says that you can set it at most once. So if you intend to support multiple custom protocols in the same application, you'd need to generify the custom URLStreamHandlerFactory implementation to cover them all inside the createURLStreamHandler() method.
    
    Alternatively, if you dislike the Law of Demeter, throw it all together in anonymous classes for code minification:
    
    ```
    URL.setURLStreamHandlerFactory(new URLStreamHandlerFactory() {
        public URLStreamHandler createURLStreamHandler(String protocol) {
            return "customuri".equals(protocol) ? new URLStreamHandler() {
                protected URLConnection openConnection(URL url) throws IOException {
                    return new URLConnection(url) {
                        public void connect() throws IOException {
                            System.out.println("Connected!");
                        }
                    };
                }
            } : null;
        }
    });
    ```

5. Finally add the registered protocol scheme to filedownloader#FileDownloaderModule#supportedSchemes.

    Don't forget to add a corresponding test-case in FileDownloaderTests just to make sure that new handler integrates 
    well with java.net.URL.

