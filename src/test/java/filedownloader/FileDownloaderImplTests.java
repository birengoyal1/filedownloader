package filedownloader;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import filedownloader.FileDownloader;
import filedownloader.FileDownloaderImpl;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class FileDownloaderImplTests {

    @Rule
    public WireMockRule mockServerRule = new WireMockRule();

    @Test
    public void isAssignabletoFileDownloder() {
        assertThat(FileDownloader.class.isAssignableFrom(FileDownloaderImpl.class), is(equalTo(true)));
    }

    @Test
    public void shouldConnectToASupportedUrl() {
        stubFor(get(urlMatching("/index.html")).willReturn(aResponse()));
        FileDownloader downloader = new FileDownloaderImpl();
        assertThat(downloader.download("http://localhost:8080/index.html"), is(notNullValue()));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionForAnUnsupportedUrl() {
        stubFor(get(urlMatching("/index.html")).willReturn(aResponse()));
        FileDownloader downloader = new FileDownloaderImpl();
        downloader.download("http://localhost:8080/some_random_thing.html");
    }

    @Test
    public void shouldDownloadTheDataProperly() throws IOException {
        String testBody = "test content.";
        stubFor(get(urlMatching("/index.html")).willReturn(aResponse().withBody(testBody)));
        FileDownloader downloader = new FileDownloaderImpl();
        BufferedReader reader =
            new BufferedReader(new InputStreamReader(
                downloader.download("http://localhost:8080/index.html")));
        String content = "";
        String line = "";
        while ((line = reader.readLine()) != null) {
            content += line;
        }
        assertThat(content, is(CoreMatchers.equalTo(testBody)));
    }
}
