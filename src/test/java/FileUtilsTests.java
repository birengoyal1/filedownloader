import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.validator.UrlValidator;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.base.Optional;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;

import filedownloader.FileDownloader;
import filedownloader.FileDownloaderImpl;

public class FileUtilsTests {

    private static final String TEST_BASE_DIR = "./data";
    private static final String TEST_BODY = "testbody";

    private FileUtils fileUtils;

    @Rule
    public WireMockRule mockServerRule = new WireMockRule();

    @Before
    public void setup() {
        Injector injector = Guice.createInjector(new TestModule());
        fileUtils = injector.getInstance(FileUtils.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUniquePathShouldThrowExceptionForEmptyUrl() {
        fileUtils.getUniquePath("", Optional.<String>absent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUniquePathShouldThrowExceptionForMalformedUrl() {
        fileUtils.getUniquePath("malformedurl", Optional.<String>absent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPathShouldThrowExceptionForUnsupportedSchemeUrls() {
        fileUtils.getUniquePath("ftp://test.com:80/test.txt", Optional.<String>absent());
    }

    @Test
    public void getUniquePathShouldReturnRightPathForWellFormedAndSupportedUrlWhenPortIsNotSpecified() {
        String testUrl = "https://raw.githubusercontent.com/neovim/neovim/master/runtime/doc/filetype.txt";
        String expectedPath =
            TEST_BASE_DIR + "/https/raw-githubusercontent-com/443/neovim/neovim/master/runtime/doc/filetype.txt";
        assertThat(fileUtils.getUniquePath(testUrl, Optional.<String>absent()), is(equalTo(expectedPath)));
    }

    @Test
    public void getUniquePathShouldReturnRightPathForWellFormedAndSupportedUrlWhenPortIsSpecified() {
        String testUrl = "https://raw.githubusercontent.com:2000/neovim/neovim/master/runtime/doc/filetype.txt";
        String expectedPath =
            TEST_BASE_DIR  + "/https/raw-githubusercontent-com/2000/neovim/neovim/master/runtime/doc/filetype.txt";
        assertThat(fileUtils.getUniquePath(testUrl, Optional.<String>absent()), is(equalTo(expectedPath)));
    }

    @Test
    public void downloadAndSaveShouldCreateDownloadFileProperlyIfPathIsNotProvided() throws IOException {
        String testUrl = "http://127.0.0.1:8080/index.html";
        File expectedFile = new File(TEST_BASE_DIR  + "/http/127-0-0-1/8080/index.html");
        expectedFile.deleteOnExit();
        expectedFile.getParentFile().deleteOnExit();
        stubFor(get(urlMatching("/index.html")).willReturn(aResponse().withBody(TEST_BODY)));
        fileUtils.downloadAndSave(testUrl, Optional.<String>absent());
        try (BufferedReader fileReader = new BufferedReader(new FileReader(expectedFile))) {
            String content = "";
            String line = "";
            while ((line = fileReader.readLine()) != null) {
                content += line;
            }
            assertThat(content, CoreMatchers.is(CoreMatchers.equalTo(TEST_BODY)));
        }
        expectedFile.delete();
    }

    @Test
    public void downloadAndSaveShouldCreateDownloadFileProperlyIfPathIsProvided() throws IOException {
        String testUrl = "http://127.0.0.1:8080/index.html";
        File expectedFile = new File("./http/127-0-0-1/8080/index.html");
        expectedFile.deleteOnExit();
        expectedFile.getParentFile().deleteOnExit();
        stubFor(get(urlMatching("/index.html")).willReturn(aResponse().withBody(TEST_BODY)));
        fileUtils.downloadAndSave(testUrl, Optional.of("."));
        try (BufferedReader fileReader = new BufferedReader(new FileReader(expectedFile))) {
            String content = "";
            String line = "";
            while ((line = fileReader.readLine()) != null) {
                content += line;
            }
            assertThat(content, CoreMatchers.is(CoreMatchers.equalTo(TEST_BODY)));
        }
    }

    class TestModule extends AbstractModule {

        @Provides
        UrlValidator provideUrlValidator() {
            return new UrlValidator(new String[] {"http", "https"});
        }

        @Provides
        @LauncherModule.BaseDir
        File getBaseDir() {
            return new File(TEST_BASE_DIR);
        }

        protected void configure() {
            bind(FileDownloader.class).to(FileDownloaderImpl.class);
        }
    }
}
