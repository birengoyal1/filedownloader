package filedownloader;

import java.io.InputStream;

public interface FileDownloader {

    /**
     * Downloads a file from URL
     *
     * @param url, the URL to download file from.
     * @return the input stream of file contents.
     */
    InputStream download(String url);
}

