package filedownloader;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class FileDownloaderImpl implements FileDownloader {

    public InputStream download(String urlStr) {
        try {
            return  (new URL(urlStr)).openStream();
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Malformed URL: " + urlStr + ": " + e.getMessage(), e);
        } catch (IOException e) {
            throw new IllegalStateException(
                "Could not establish connection to the URL: " + urlStr + ": " + e.getMessage(), e);
        }
    }
}
