package filedownloader;

import com.google.common.collect.ImmutableSet;
import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Set;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

public class FileDownloaderModule extends AbstractModule {

    protected void configure() {
        bind(FileDownloader.class).to(FileDownloaderImpl.class);
    }

    @Provides
    @SupportedSchemes
    Set<String> provideSupportedSchemes() {
        return ImmutableSet.of("file", "ftp", "http", "https", "jar", "mailto", "netdoc");
    }

    @BindingAnnotation
    @Target( {FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public @interface SupportedSchemes {
    }
}
