import com.google.common.base.Optional;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

import java.io.IOException;
import java.util.List;

/**
 * File download launcher..
 */
public class FileDownloaderLauncher {

   private static final Logger logger = LogManager.getLogger(FileDownloaderLauncher.class);

   @Option(name = "--urls", handler = StringArrayOptionHandler.class, required = true, usage = "The url to download file from.")
   private static List<String> urls;

   @Option(name = "--location", usage = "The location to save file to. Structure inside location is decided by {@FileUtils#getUniquePath)")
   private static String baseDir = "";

   public static void main(String args[]) throws CmdLineException, IOException {
      System.setProperty(
               "http.agent",
               "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
      BasicConfigurator.configure();

      (new FileDownloaderLauncher()).parseArgs(args);

      Injector injector = Guice.createInjector(new LauncherModule());

      FileUtils fileUtils = injector.getInstance(FileUtils.class);
      for (String url : urls) {
         logger.info("Downloading file from URL: " + url);
         try {
            fileUtils.downloadAndSave(url, "".equals(baseDir) ? Optional.<String> absent()
                     : Optional.of(baseDir));
         } catch (Exception e) {
            logger.error("Failed to download file from- " + url + ":  " + e.getMessage());
         }
      }
   }

   public void parseArgs(String args[]) throws CmdLineException {
      CmdLineParser parser = new CmdLineParser(this);
      parser.parseArgument(args);
   }
}
