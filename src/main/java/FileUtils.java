import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import filedownloader.FileDownloader;
import org.apache.commons.io.IOUtils;
import org.apache.commons.validator.UrlValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

class FileUtils {

    private static final Logger logger = LogManager.getLogger(FileDownloaderLauncher.class);

    @Inject
    @LauncherModule.BaseDir
    File defaultBaseDir;

    @Inject
    UrlValidator urlValidator;

    @Inject
    FileDownloader fileDownloader;

    /**
     * Gets the unique path to save file at.
     *
     * <p>
     *     Example URL: https://raw.githubusercontent.com/neovim/neovim/master/runtime/doc/filetype.txt
     *
     *     Downloaded file will be saved at: {{@link #defaultBaseDir}}/<protocol>/<host>/<port>/<file>
     *     where protocol is the protocol of the URL. From example: https
     *     host is the host of the protocol. From example: raw.githubusercontent.com
     *     file is the filename in the URL. From example: neovim/neovim/master/runtime/doc/filetype.txt
     *
     *     All the dots(.) in the host will be replaced by dash(-).
     *
     *     Example final path from example URL:
     *     {{@link #defaultBaseDir}}/https/raw-githubusercontent-com/80/neovim/neovim/master/runtime/doc/filetype.txt
     *
     *     <i>Note that: 80 is the default port in http. </i>
     * </p>
     *
     * @param urlStr, the URL file belongs to.
     * @param baseDir, the Base directory to create file in.
     * @return path to store file at in the localhost.
     */
    String getUniquePath(String urlStr, Optional<String> baseDir) {
        Preconditions.checkArgument(
            urlValidator.isValid(urlStr), "Given URL is either not supported or malformed: " + urlStr);
        try {
            URL url = new URL(urlStr);
            String baseDirectory = baseDir.isPresent() ? baseDir.get() : defaultBaseDir.getPath();
            return baseDirectory + File.separator
                + url.getProtocol() + File.separator
                + url.getHost().replace(".", "-") + File.separator
                + (url.getPort() != -1 ? url.getPort() : url.getDefaultPort())
                + url.getPath();
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Given URL is malformed " + urlStr + ": " + e.getMessage(), e);
        }
    }

    /**
     * Downloads and saves the file at given URL to the given or default path.
     *
     * @param url,  the URL.
     * @param baseDir, base dir to save file to.
     */
    void downloadAndSave(String url, Optional<String> baseDir) {
        String downloadPath = getUniquePath(url, baseDir);
        logger.info("Downloading file at: " + downloadPath);
        File file = new File(downloadPath);
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(
                    "Could not create directory at: " + file.getParentFile().getPath() + ": " + e.getMessage(), e);
            }
        }
        try {
            IOUtils.copyLarge(fileDownloader.download(url), new FileOutputStream(file));
            
        } catch (IOException e) {
            throw new IllegalStateException("Could not write file: " + file.getPath() + ": " + e.getMessage(), e);
        }
        logger.info("saved file");
    }
}
