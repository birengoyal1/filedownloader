import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import filedownloader.FileDownloaderModule;
import org.apache.commons.validator.UrlValidator;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.Set;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

public class LauncherModule extends AbstractModule {

    private static final String APP_NAME = "filedownloader";

    protected void configure() {
        install(new FileDownloaderModule());
    }

    @Provides
    @BaseDir
    File baseDirProvider() {
        return new File(System.getProperty("user.home") + File.separator + APP_NAME);
    }

    @Provides
    UrlValidator provideUrlValidator(@FileDownloaderModule.SupportedSchemes Set<String> supportedSchemes) {
        return new UrlValidator(supportedSchemes.toArray(new String[supportedSchemes.size()]));
    }

    @BindingAnnotation
    @Target( {FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public @interface BaseDir {
    }
}
